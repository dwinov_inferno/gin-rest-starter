package config

import (
	"github.com/joho/godotenv"
	"github.com/joeshaw/envdecode"
	"github.com/pkg/errors"
)

type Config struct {
	Env         string `env:"ENV,default=development"`
	ServiceName string `env:"SERVICE_NAME,default=grpc-starter"`
	Port        Port
	Postgres    Postgres
	Redis       Redis
	JWTToken    JWTToken
}

type Port struct {
	PORT string `env:"PORT,default=8080"`
}

type Postgres struct {
	Host            string `env:"DB_HOST,default=localhost"`
	Port            string `env:"DB_PORT,default=5432"`
	User            string `env:"DB_USER,required"`
	Password        string `env:"DB_PASSWORD,required"`
	Name            string `env:"DB_NAME,required"`
	MaxOpenConns    int    `env:"DB_MAX_OPEN_CONNS,default=5"`
	MaxConnLifetime int    `env:"DB_MAX_CONN_LIFETIME,default=10m"`
	MaxIdleLifetime int    `env:"DB_MAX_IDLE_LIFETIME,default=5m"`
}

type Redis struct {
	Address  string `env:"REDIS_ADDRESS,required"`
	Password string `env:"REDIS_PASSWORD"`
}

type JWTToken struct {
	Secret string `env:"JWT_SECRET_KEY,required"`
}

func LoadConfig(env string) (*Config, error) {
	_ = godotenv.Load(env)

	var config Config
	if err := envdecode.Decode(&config); err != nil {
		return nil, errors.Wrap(err, "[NewConfig] error decoding env")
	}

	return &config, nil
}
