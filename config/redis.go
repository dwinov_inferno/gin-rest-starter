package config

import (
	"context"
	"github.com/go-redis/redis/v8"
)

var RedisClient *redis.Client

func ConnectRedis(config *Config) error {
	RedisClient = redis.NewClient(&redis.Options{
		Addr:     config.Redis.Address,
		Password: config.Redis.Password,
		DB:       0,
	})

	ctx := context.Background()
	_, err := RedisClient.Ping(ctx).Result()
	if err != nil {
		return err
	}

	return nil
}
