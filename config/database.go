package config

import (
	"fmt"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var DB *gorm.DB

func ConnectDatabase(config *Config) error {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Shanghai",
		config.Postgres.Host, config.Postgres.User, config.Postgres.Password, config.Postgres.Name, config.Postgres.Port)
	var err error

	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	if err != nil {
		return err
	}

	sqlDB, err := DB.DB()
	if err != nil {
		return err
	}

	sqlDB.SetMaxOpenConns(config.Postgres.MaxOpenConns)
	sqlDB.SetConnMaxLifetime(time.Duration(config.Postgres.MaxConnLifetime) * time.Minute)
	sqlDB.SetConnMaxIdleTime(time.Duration(config.Postgres.MaxIdleLifetime) * time.Minute)

	return nil
}
