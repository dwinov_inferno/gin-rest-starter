package tools

import "golang.org/x/crypto/bcrypt"

const (
	cost = 10
)

// BcryptEncrypt encrypts a string.
func BcryptEncrypt(plainText string) (string, error) {
	hashed, err := bcrypt.GenerateFromPassword([]byte(plainText), cost)
	return string(hashed), err
}

// BcryptVerifyHash compares hashed and plain string.
func BcryptVerifyHash(encrypted, plain string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(encrypted), []byte(plain))
	return err == nil
}
