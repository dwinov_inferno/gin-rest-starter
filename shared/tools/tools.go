package tools

import (
	"regexp"
	"strings"
)

// special chars [~!@#$%^&*()-_+={}[]|\/:;"'<>,.?]
var specialChars = regexp.MustCompile(`[[^A-Za-z0-9]]`)

// EscapeSpecial and Literal escape the given string.
func EscapeSpecial(s string) string {
	matches := specialChars.FindAllStringSubmatch(s, -1)

	for _, match := range matches {
		s = strings.ReplaceAll(s, match[0], ``)
	}

	return s
}
