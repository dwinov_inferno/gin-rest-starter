package utils

import "github.com/gin-gonic/gin"

type CustomError struct {
	Code    int
	Message string
}

func NewCustomError(code int, message string) *CustomError {
	return &CustomError{Code: code, Message: message}
}

func (e *CustomError) Error() string {
	return e.Message
}

func ErrorHandler(c *gin.Context, err error) {
	if customErr, ok := err.(*CustomError); ok {
		c.JSON(customErr.Code, gin.H{"error": customErr.Message})
	} else {
		c.JSON(500, gin.H{"error": "Internal Server Error"})
	}
}
