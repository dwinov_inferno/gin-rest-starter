package utils

import (
	"context"
	"time"
	"github.com/go-redis/redis/v8"
	"gin-restful-starter/config"
)

// SetRedis untuk menetapkan nilai di Redis
func SetRedis(key string, value interface{}, expiration time.Duration) error {
	ctx := context.Background()
	err := config.RedisClient.Set(ctx, key, value, expiration).Err()
	if err != nil {
		return NewCustomError(500, "Error setting value in Redis")
	}
	return nil
}

// GetRedis untuk mendapatkan nilai dari Redis
func GetRedis(key string) (string, error) {
	ctx := context.Background()
	val, err := config.RedisClient.Get(ctx, key).Result()
	if err == redis.Nil {
		return "", NewCustomError(404, "Key not found in Redis")
	} else if err != nil {
		return "", NewCustomError(500, "Error getting value from Redis")
	}
	return val, nil
}

// DeleteRedis untuk menghapus nilai dari Redis
func DeleteRedis(key string) error {
	ctx := context.Background()
	err := config.RedisClient.Del(ctx, key).Err()
	if err != nil {
		return NewCustomError(500, "Error deleting value from Redis")
	}
	return nil
}
