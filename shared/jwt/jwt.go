package jwt

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"os"
	"time"
)


// CustomClaims define available data in JWT
type CustomClaims struct {
	ExpiresAt int64     `json:"exp,omitempty"`
	ID        string    `json:"jti,omitempty"`
	IssuedAt  int64     `json:"iat,omitempty"`
	NotBefore int64     `json:"nbf,omitempty"`
	Subject   uuid.UUID `json:"sub,omitempty"`
	Issuer    string    `json:"iss,omitempty"`
	jwt.StandardClaims
}

// verifyClaims verifies the claims in the token.
func verifyClaims(accessToken string) (*CustomClaims, error) {
	token, err := jwt.ParseWithClaims(
		accessToken,
		&CustomClaims{},
		func(token *jwt.Token) (interface{}, error) {
			_, ok := token.Method.(*jwt.SigningMethodHMAC)
			if !ok {
				return nil, fmt.Errorf("unexpected token signing method")
			}

			return []byte(os.Getenv("JWT_SECRET_KEY")), nil
		},
	)

	if err != nil {
		return nil, fmt.Errorf("invalid token: %w", err)
	}

	claims, ok := token.Claims.(*CustomClaims)
	if !ok {
		return nil, fmt.Errorf("invalid token claims")
	}

	return claims, nil
}

// getTokenRemainingValidity for check expired time
func getTokenRemainingValidity(timestamp interface{}) bool {
	timeExp := time.Unix(timestamp.(int64), 0)
	remainer := time.Until(timeExp)
	return remainer > 0
}
