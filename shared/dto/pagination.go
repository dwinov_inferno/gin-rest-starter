package dto

type Pagination struct {
	Search      string `json:"search"`
	Page        uint32 `json:"page"`
	Limit       uint32 `json:"limit"`
	OrderBy     string `json:"order_by"`
	OrderColumn string `json:"order_column"`
}

type SuccessJSON struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type SuccessJSONDatatable struct {
	TotalPage uint32      `json:"total_page"`
	Count     uint32      `json:"count"`
	Page      uint32      `json:"page"`
	Data      interface{} `json:"data"`
}
