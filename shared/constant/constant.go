package constant

const (
	// SuccessMessage define success message
	SuccessMessage = "success"
	// Ascending define ascending constant
	Ascending = "asc"
	// Descending define descending constant
	Descending = "desc"
	// DefaultLimit default limit for pagination
	DefaultLimit = 10
	// Zero define number 0
	Zero = 0
	// One define number one
	One = 1
	// Two define number two
	Two = 2
	// Three define number three
	Three = 3
	// Four define number four
	Four = 4
	// Five define number five
	Five = 5
	// Six define number six
	Six = 6
	// Seven define number six
	Seven = 7
	// MinusSixPointTwo define number six
	MinusSixPointTwo = -6.2
	// MinusSeven define number minus seven
	MinusSeven = -7
	// Ten define number ten
	Ten = 10
	// MinusTen define number ten
	MinusTen = -10
	// MinusTenPointFive define number ten point five
	MinusTenPointFive = -10.5
	// Eleven define number Eleven
	Eleven = 11
	// MinusEleven define number Eleven
	MinusEleven = -11
	// Twelve define number Twelve
	Twelve = 12
	// MinusTwelve define number Twelve
	MinusTwelve = -12
	// FiveTeen define number five teen
	FiveTeen = 15
	// NineTeen define number nine teen
	NineTeen = 19
	// ThirtyTwo define number thirty two
	ThirtyTwo = 32
	// TwentyFourHour define twenty-four hours
	TwentyFourHour = 24
	// DaysInOneYear define days in one year
	DaysInOneYear = 365
	// Thirty define number thirty
	Thirty = 30
	// Fifty define number fifty
	Fifty = 50
	// Hundred define number hundred
	Hundred = 100
	// Thousand define number thousand
	Thousand = 1000
	// MinusThousand define number thousand
	MinusThousand = -1000
	// NinetyNineHundred define number ninety nine hundred
	NinetyNineHundred = 9999
	// TenThousand define number ten thousand
	TenThousand = 10000
	// PermissionFile define number for permission file
	PermissionFile = 0600
	// OneHourInSecond define one hour in second
	OneHourInSecond = 3600
	// HalfHourInSecond define half hour in second
	HalfHourInSecond = 1800
	// OneYearInSecond define one year in second
	OneYearInSecond = 31556952
	// OneHourInMinute define one hour in second
	OneHourInMinute = 60
	// Twenty define number twenty
	Twenty = 20
	// MinusFive define number twenty
	MinusFive = -5
	// Eight define number twenty
	Eight = 8
	// ThreeHundredAndTwenty define number ThreeHundredAndTwenty
	ThreeHundredAndTwenty = 320
	// ThirtyOne define number thirty
	ThirtyOne = 31
	// OneHundredEightySix define number thirty
	OneHundredEightySix = 186
	// ThirtyOne define number thirty
	MinusTwenty = -20
	// MobileIssuer define mobile issuer
	MobileIssuer = "gin-rest-starter.mobile"
	// MobileIssuer define mobile issuer
	WebIssuer = "gin-rest-starter.web"
)
