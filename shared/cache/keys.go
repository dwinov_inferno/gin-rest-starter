package cache

const (
	prefix = "gin-restful-starter"

	UserFindByID = prefix + ":user:find-by-id:%v"
	UserFindByEmail = prefix + ":user:find-by-email:%v"
	UserDatatable = prefix + ":user:find-all-datatable:%v:%v:%v:%v:%v"

	RoleFindByID = prefix + ":role:find-by-id:%v"
	RoleDatatable = prefix + ":role:find-all-datatable:%v:%v:%v:%v:%v"
)
