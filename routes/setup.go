package routes

import (
	"gin-restful-starter/config"
	authRoute "gin-restful-starter/modules/authentication/routes"
	userRoute "gin-restful-starter/modules/user/routes"
	"github.com/gin-gonic/gin"
	"github.com/gomodule/redigo/redis"
	"gorm.io/gorm"
)

func SetupRouter(cfg config.Config, db *gorm.DB, rdb *redis.Pool) *gin.Engine {
	router := gin.Default()

	// Daftarkan routes dari setiap module
	authRoute.AuthenticationRoutes(cfg, router, db, rdb)
	userRoute.UserRoutes(router, db, rdb)

	return router
}
