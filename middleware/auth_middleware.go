package middleware

import (
	"fmt"
	"gin-restful-starter/config"
	"gin-restful-starter/errors"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"

	"gin-restful-starter/modules/authentication/repository"
)

func AuthMiddleware(userRepo repository.AuthRepository) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")
		if authHeader == "" {
			err := errors.NewCustomError(http.StatusUnauthorized, "missing authorization header")
			c.JSON(http.StatusUnauthorized, gin.H{"error": err.Message})
			c.Abort()
			return
		}

		authParts := strings.Split(authHeader, "Bearer ")
		if len(authParts) != 2 {
			err := errors.NewCustomError(http.StatusUnauthorized, "invalid Authorization header format")
			c.JSON(http.StatusUnauthorized, gin.H{"error": err.Message})
			c.Abort()
			return
		}

		tokenString := authParts[1]
		token, err := verifyToken(tokenString)
		if err != nil {
			code, message := errors.HandleError(err)
			c.JSON(code, gin.H{"error": message})
			c.Abort()
			return
		}

		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			err := errors.NewCustomError(http.StatusUnauthorized, "invalid token")
			c.JSON(http.StatusUnauthorized, gin.H{"error": err.Message})
			c.Abort()
			return
		}

		// Extract user ID and role ID from claims
		userID, ok := claims["sub"].(string)
		if !ok {
			err := errors.NewCustomError(http.StatusUnauthorized, "invalid token: missing subject")
			c.JSON(http.StatusUnauthorized, gin.H{"error": err.Message})
			c.Abort()
			return
		}

		// get role id
		dataUser, err := userRepo.FindByID(userID)
		if err != nil {
			code, message := errors.HandleError(err)
			c.JSON(code, gin.H{"error": message})
			c.Abort()
			return
		}

		// Set user ID and role ID in context for use in subsequent handlers
		c.Set("userID", userID)
		c.Set("roleID", dataUser.RoleID)

		c.Next()
	}
}

func verifyToken(tokenString string) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Check token signing method
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(config.JWTToken{}.Secret), nil
	})
	if err != nil {
		return nil, err
	}
	return token, nil
}
