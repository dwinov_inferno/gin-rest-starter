## Database Migration

Database migration is used to migrate database definition to the actual database.
The migration files are located in directory [db/migrations](../db/migrations).
Each module has its own directory inside [db/migrations](../db/migrations).
Each module must define its database migration inside its own directory. See [db/migrations/assessment](../db/migrations/assessment) for example. 

We use different schema for different module. The schemas are defined in [db/schemas](../db/schemas).

### General Rule

#### Setup
- install `golang-migrate` for migration
- Global (via brew di macOS or apt-get di Linux):
    ```
    brew install golang-migrate
    ```
- Or Via go module:
    ```
    go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest
    ```
- to create new table, you can use this command:
    ```
    migrate create -ext sql -dir ./migrations/tables -seq create_table_users
    ```

### Schema

Before creating table for your module's use case, you **MUST** define its schema. The schema's name must be the same as module's name. See [db/schemas/assessment](../db/schemas/000001_assessment.up.sql) for example.

Always remember to create your schema first before creating table migrations.

To create schema, run this command:

```
migrate create -ext sql -dir ./migrations/schemas -seq create_schema_your_schema
```
The command will create 2 files in tables directory `000001_create_schema_your_schema.up.sql` and `000001_create_schema_your_schema.down.sql`

Fill the file with commands like:
    
    - Up file:
    CREATE SCHEMA your_schema;
    
    - Down file:
    DROP SCHEMA your_schema CASCADE;
    
Then, command for migration. These three command must success.

```
migrate create -ext sql -dir ./migrations/tables -seq create_table_users
```

### Table

After creating schema for your module's use case, you can create the table migration.

To create table, run this command:

```
migrate create -ext sql -dir ./migrations/tables -seq create_table_users
```

The command will create 2 files in tables directory `000001_create_table_users.up.sql` and `000001_create_table_users.down.sql`

Fill the file with commands like:
    
    - Up file:
    CREATE TABLE your_schema.users (
        id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
        name VARCHAR(100) NOT NULL,
        email VARCHAR(100) UNIQUE NOT NULL,
        password VARCHAR(100) NOT NULL,
        created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP
    );
    
    - Down file:
    DROP TABLE your_schema.users;
    
Then, command for migration. These three command must success.

```
migrate create -ext sql -dir ./migrations/tables -seq create_table_users
```

### Migration

After creating schema and/or table, it's time to define your table migration.

To create migration, run this command for schema:
```
migrate -path ./migrations/schemas -database "postgres://your_username:your_password@localhost:5432/your_database?sslmode=disable" up
```
Run this command for table:
```
migrate -path ./migrations/tables -database "postgres://your_username:your_password@localhost:5432/your_database?sslmode=disable" up
```
