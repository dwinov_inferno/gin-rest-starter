package handler

import (
	"gin-restful-starter/modules/authentication/entity"
	"gin-restful-starter/shared/constant"
	"gin-restful-starter/shared/dto"
	"gin-restful-starter/shared/errors"
	"gin-restful-starter/shared/utils"
	"github.com/gin-gonic/gin"
	"gin-restful-starter/modules/authentication/service"
	"google.golang.org/grpc/status"
	"net/http"
)

type AuthHandler struct {
	authService service.AuthService
}

func NewAuthHandler(authService service.AuthService) *AuthHandler {
	return &AuthHandler{authService: authService}
}

func (h *AuthHandler) SetupAuthRoutes(router *gin.Engine) {
	auth := router.Group("/auth")
	{
		auth.POST("/login", h.Login)
		auth.POST("/register", h.Register)
	}
}

func (h *AuthHandler) Login(c *gin.Context) {
	ctx := c.Request.Context()
	var req entity.LoginRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		utils.ErrorHandler(c, utils.NewCustomError(400, "Invalid request"))
		return
	}

	platform := c.GetHeader("X-Platform")
	if platform == "" {
		utils.ErrorHandler(c, utils.NewCustomError(400, "Platform is required"))
		return
	}

	user, err := h.authService.Login(ctx, &req, platform)
	if err != nil {
		utils.ErrorHandler(c, err)
		return
	}

	if user == "" {
		utils.ErrorHandler(c, status.Errorf(errors.ErrWrongLoginCredentials.Code, errors.ErrWrongLoginCredentials.Message))
		return
	}

	c.JSON(http.StatusCreated, dto.SuccessJSON{
		Code: http.StatusOK,
		Message: constant.SuccessMessage,
	})
}

func (h *AuthHandler) Register(c *gin.Context) {
	ctx := c.Request.Context()
	var req entity.RegisterRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		utils.ErrorHandler(c, utils.NewCustomError(400, "Invalid request"))
		return
	}

	user, token, err := h.authService.Register(ctx, &req)
	if err != nil {
		utils.ErrorHandler(c, err)
		return
	}

	c.JSON(http.StatusCreated, dto.SuccessJSON{
		Code: http.StatusOK,
		Message: constant.SuccessMessage,
		Data: entity.RegisterResponse{
			ID: user.ID.String(),
			Token: token,
		},
	})
}
