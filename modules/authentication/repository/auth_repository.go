package repository

import (
	userEntity "gin-restful-starter/modules/user/entity"
	"gin-restful-starter/shared/utils"
	"gorm.io/gorm"
)

type AuthRepository interface {
	FindByID(id string) (*userEntity.User, error)
	GetUserByEmail(email string) (*userEntity.User, error)
}

type authRepository struct {
	db *gorm.DB
}

func NewAuthRepository(db *gorm.DB) AuthRepository {
	return &authRepository{db: db}
}

func (r *authRepository) FindByID(id string) (*userEntity.User, error) {
	var user userEntity.User
	if err := r.db.Where("id = ?", id).First(&user).Error; err != nil {
		return nil, utils.NewCustomError(404, "User not found")
	}
	return &user, nil
}

func (r *authRepository) GetUserByEmail(email string) (*userEntity.User, error) {
	var user userEntity.User
	if err := r.db.Where("email = ?", email).First(&user).Error; err != nil {
		return nil, utils.NewCustomError(404, "User not found")
	}
	return &user, nil
}
