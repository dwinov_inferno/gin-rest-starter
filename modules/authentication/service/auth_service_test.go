package service_test

import (
	"context"
	"gin-restful-starter/config"
	"gin-restful-starter/modules/user/repository"
	sharedError "gin-restful-starter/shared/errors"
	"gin-restful-starter/shared/tools"
	"github.com/google/uuid"
	"testing"

	"github.com/stretchr/testify/assert"

	"gin-restful-starter/modules/authentication/entity"
	"gin-restful-starter/modules/authentication/service"
	userEntity "gin-restful-starter/modules/user/entity"
)

func TestLogin(t *testing.T) {
	ctx := context.Background()
	encrypted, _ := tools.BcryptEncrypt("testpassword")
	cfg := config.Config{}

	// Setup mock user
	mockedUser := userEntity.User{
		ID:       uuid.New(),
		Email:    "test@example.com",
		Password: encrypted, // Hash password beforehand
	}

	// Setup mock user finder service
	mockUserService := new(repository.UserFinderServiceMock)
	mockUserService.On("GetUserByEmail", context.Background(), "test@example.com").Return(mockedUser, nil)

	// Setup AuthService with mock
	authService := service.NewAuthService(cfg, nil, nil, mockUserService)

	// Test case: User tidak ditemukan
	t.Run("UserNotFound", func(t *testing.T) {
		mockUserService.On("GetUserByEmail", ctx, "nonexistent@example.com").Return(userEntity.User{}, sharedError.ErrRecordNotFound.Error())
		_, err := authService.Login(ctx, &entity.LoginRequest{Email: "nonexistent@example.com", Password: "password"}, "web")

		assert.NotNil(t, err)
		assert.Nil(t, nil)
	})

	// Test case: Password salah
	t.Run("WrongPassword", func(t *testing.T) {
		plainPassword := "WrongPassword"

		mockUserService.On("GetUserByEmail", ctx, "test@example.com").Return(mockedUser, nil)

		result := tools.BcryptVerifyHash(encrypted, plainPassword)
		assert.False(t, result, "Password should not match")

		_, err := authService.Login(ctx, &entity.LoginRequest{Email: "test@example.com", Password: "wrong_password"}, "web")

		assert.NotNil(t, err)
		assert.Nil(t, nil)
	})

	// Test case: Login berhasil
	t.Run("SuccessfulLogin", func(t *testing.T) {
		plainPassword := "testpassword"

		mockUserService.On("GetUserByEmail", ctx, "test@example.com").Return(mockedUser, nil)

		result := tools.BcryptVerifyHash(encrypted, plainPassword)
		assert.True(t, result, "Password should match")

		token, err := authService.Login(ctx, &entity.LoginRequest{Email: "test@example.com", Password: plainPassword}, "web")

		assert.Nil(t, err)
		assert.NotNil(t, token)
	})
}


