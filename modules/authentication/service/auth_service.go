package service

import (
	"context"
	"gin-restful-starter/config"
	"gin-restful-starter/modules/authentication/repository"
	"gin-restful-starter/modules/authentication/entity"
	userEntity "gin-restful-starter/modules/user/entity"
	"gin-restful-starter/shared/constant"
	"gin-restful-starter/shared/tools"
	"gin-restful-starter/shared/errors"
	commonJwt "gin-restful-starter/shared/jwt"
	commonError "gin-restful-starter/shared/errors"
	UserService "gin-restful-starter/modules/user/service"
	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"log"
	"time"
)

type AuthService interface {
	Login(ctx context.Context, req *entity.LoginRequest, platform string) (string, error)
	Register(ctx context.Context, req *entity.RegisterRequest) (*userEntity.User, string, error)
}

type authService struct {
	config             config.Config
	authRepository     repository.AuthRepository
	userCreatorService UserService.UserCreatorService
	userFinderService  UserService.UserFinderService
}

func NewAuthService(
	config config.Config,
	authRepository repository.AuthRepository,
	userCreatorService UserService.UserCreatorService,
	userFinderService UserService.UserFinderService,
) AuthService {
	return &authService{
		config:             config,
		authRepository:     authRepository,
		userCreatorService: userCreatorService,
		userFinderService:  userFinderService,
	}
}

func (svc *authService) Login(ctx context.Context, req *entity.LoginRequest, platform string) (string, error) {
	user, err := svc.userFinderService.GetUserByEmail(ctx, req.Email)
	if err != nil {
		log.Print("[AuthService - Login] Error while finding user by Email: ", err)
		return "", err
	}

	if user == nil {
		return "", nil
	}

	verifyPassword := tools.BcryptVerifyHash(user.Password, req.Password)
	if !verifyPassword {
		log.Print("[AuthService - Login] Error while verifying password: ", err)
		return "", errors.ErrWrongLoginCredentials.Error()
	}

	issuer := constant.WebIssuer
	if platform == "mobile" {
		issuer = constant.MobileIssuer
	}

	claims := &commonJwt.CustomClaims{
		ExpiresAt: time.Now().Add(constant.Thirty * time.Minute).Unix(),
		ID:        uuid.New().String(),
		IssuedAt:  time.Now().Unix(),
		NotBefore: time.Now().Unix(),
		Subject:   user.ID,
		Issuer:    issuer,
	}

	gen := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	token, err := gen.SignedString([]byte(svc.config.JWTToken.Secret))
	if err != nil {
		log.Println("[UserFinder - Login] Error while generating token :", err)
		return "", errors.ErrInternalServerError.Error()
	}

	return token, nil
}

func (svc *authService) Register(ctx context.Context, req *entity.RegisterRequest) (*userEntity.User, string, error) {
	user, err := svc.userFinderService.GetUserByEmail(ctx, req.Email)
	if err != nil {
		log.Print("[UserCreator - Create] Error while get user by email: ", err)
		return nil, "", commonError.ErrInternalServerError.Error()
	}

	if user != nil {
		return nil, "", commonError.ErrEmailAlreadyExists.Error()
	}

	encrypted, _ := tools.BcryptEncrypt(req.Password)
	newUUID := uuid.New()

	newUser := userEntity.User{
		ID:          newUUID,
		Username:    req.Name,
		Email:       req.Email,
		Password:    encrypted,
		PhoneNumber: req.PhoneNumber,
		CreatedAt:   time.Now(),
		CreatedBy:   newUUID.String(),
		UpdatedAt:   time.Now(),
		UpdatedBy:   newUUID.String(),
	}

	if err := svc.userCreatorService.CreateUser(ctx, &newUser); err != nil {
		log.Print("[UserCreator - Register] Error while creating user data: ", err)
		return nil, "", commonError.ErrInternalServerError.Error()
	}

	claims := &commonJwt.CustomClaims{
		ExpiresAt: time.Now().Add(time.Hour * constant.TwentyFourHour * constant.DaysInOneYear).Unix(),
		ID:        uuid.New().String(),
		IssuedAt:  time.Now().Unix(),
		NotBefore: time.Now().Unix(),
		Subject:   newUser.ID,
		Issuer:    constant.MobileIssuer,
	}

	gen := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	token, err := gen.SignedString([]byte(svc.config.JWTToken.Secret))

	if err != nil {
		log.Print("[UserCreator - Register] Error while generating token for user :", err)
		return nil, "", commonError.ErrInternalServerError.Error()
	}

	return &newUser, token, nil
}
