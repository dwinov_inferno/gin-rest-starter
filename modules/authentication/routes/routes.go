package routes

import (
	"gin-restful-starter/config"
	authHandle "gin-restful-starter/modules/authentication/handler"
	authRepository "gin-restful-starter/modules/authentication/repository"
	authServ "gin-restful-starter/modules/authentication/service"
	"gin-restful-starter/modules/user/repository"
	userService "gin-restful-starter/modules/user/service"
	commonredis "gin-restful-starter/shared/redis"
	"github.com/gin-gonic/gin"
	"github.com/gomodule/redigo/redis"
	"gorm.io/gorm"
)

func AuthenticationRoutes(cfg config.Config, router *gin.Engine, db *gorm.DB, redisPool *redis.Pool) {
	// Cache
	cache := commonredis.NewClient(redisPool)

	// Initialize auth repositories
	authRepo := authRepository.NewAuthRepository(db)
	userCreatorRepo := repository.NewUserCreatorRepository(db, cache)
	userFinderRepo := repository.NewUserFinderRepository(db, cache)
	userDeleterRepository := repository.NewUserDeleterRepository(db, cache)

	// Initialize auth services
	userCreatorService := userService.NewUserCreatorService(userCreatorRepo, userDeleterRepository)
	userFinderService := userService.NewUserFinderService(userFinderRepo)
	authService := authServ.NewAuthService(cfg, authRepo, userCreatorService, userFinderService)

	// Initialize auth handlers
	authHandler := authHandle.NewAuthHandler(authService)

	auth := router.Group("/auth")
	{
		auth.POST("/login", authHandler.Login)
		auth.POST("/register", authHandler.Register)
	}
}
