package repository

import (
	"context"
	"gin-restful-starter/modules/user/entity"
	"gin-restful-starter/shared/cache"
	"gin-restful-starter/shared/utils"
	"gorm.io/gorm"
)

type RoleUpdaterRepository interface {
	UpdateRole(context.Context, string, *entity.Role) error
}

type roleUpdaterRepository struct{
	db *gorm.DB
	cache cache.Cacheable
}

func NewRoleUpdaterRepository(
	db *gorm.DB,
	cache cache.Cacheable,
) RoleUpdaterRepository {
	return &roleUpdaterRepository{
		db: db,
		cache: cache,
	}
}

func (u *roleUpdaterRepository) UpdateRole(ctx context.Context, id string, req *entity.Role) error {
	if err := u.db.WithContext(ctx).Where("id = ?", id).Updates(&req).Error; err != nil {
		return utils.NewCustomError(500, "Error updating role")
	}
	return nil
}
