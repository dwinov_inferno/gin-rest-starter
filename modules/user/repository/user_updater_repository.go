package repository

import (
	"context"
	"gin-restful-starter/modules/user/entity"
	"gin-restful-starter/shared/cache"
	"gin-restful-starter/shared/utils"
	"gorm.io/gorm"
)

type UserUpdaterRepository interface {
	UpdateUser(context.Context, string, *entity.User) error
}

type userUpdaterRepository struct{
	db *gorm.DB
	cache cache.Cacheable
}

func NewUserUpdaterRepository(db *gorm.DB, cache cache.Cacheable) UserUpdaterRepository {
	return &userUpdaterRepository{
		db: db,
		cache: cache,
	}
}

func (u *userUpdaterRepository) UpdateUser(ctx context.Context, id string, req *entity.User) error {
	if err := u.db.WithContext(ctx).Where("id = ?", id).Updates(&req).Error; err != nil {
		return utils.NewCustomError(500, "Error updating user")
	}
	return nil
}
