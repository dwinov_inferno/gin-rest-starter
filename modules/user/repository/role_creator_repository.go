package repository

import (
	"context"
	customErrors "gin-restful-starter/errors"
	"gin-restful-starter/modules/user/entity"
	"gin-restful-starter/shared/cache"
	"gorm.io/gorm"
)

type RoleCreatorRepository interface {
	CreateRole(ctx context.Context, user *entity.Role) error
}

type roleCreatorRepository struct{
	db *gorm.DB
	cache cache.Cacheable
}

func NewRoleCreatorRepository(
	db *gorm.DB,
	cache cache.Cacheable,
) RoleCreatorRepository {
	return &roleCreatorRepository{
		db: db,
		cache: cache,
	}
}

func (u *roleCreatorRepository) CreateRole(ctx context.Context, user *entity.Role) error {
	if err := u.db.WithContext(ctx).Create(user).Error; err != nil {
		return customErrors.NewCustomError(500, err.Error())
	}

	return nil
}
