package repository

import (
	"context"
	"fmt"
	"gin-restful-starter/modules/user/entity"
	"gin-restful-starter/shared/cache"
	"gin-restful-starter/shared/utils"
	"gorm.io/gorm"
)

type RoleDeleterRepository interface {
	DeleteRole(context.Context, string) error
	DeleteAllRoleCaches(refID string) error
}

type roleDeleterRepository struct{
	db *gorm.DB
	cache cache.Cacheable
}

func NewRoleDeleterRepository(
	db *gorm.DB,
	cache cache.Cacheable,
) RoleDeleterRepository {
	return &roleDeleterRepository{
		db: db,
		cache: cache,
	}
}

func (u *roleDeleterRepository) DeleteRole(ctx context.Context, id string) error {
	if err := u.db.WithContext(ctx).Where("id = ?", id).Delete(&entity.Role{}).Error; err != nil {
		return utils.NewCustomError(500, "Error deleting user")
	}
	return nil
}

func (u *roleDeleterRepository) DeleteAllRoleCaches(refID string) error {
	err := u.cache.BulkRemove(fmt.Sprintf(cache.RoleDatatable, "*", "*", "*", "*", "*"))
	if err != nil {
		return err
	}

	err = u.cache.BulkRemove(fmt.Sprintf(cache.RoleDatatable, "*", "*", "*", "*", "*"))
	if err != nil {
		return err
	}

	if refID != "" {
		err = u.cache.Remove(fmt.Sprintf(cache.RoleFindByID, refID))
		if err != nil {
			return err
		}
	}

	return nil
}
