package repository

import (
	"context"
	"fmt"
	"gin-restful-starter/modules/user/entity"
	"gin-restful-starter/shared/cache"
	"gin-restful-starter/shared/utils"
	"gorm.io/gorm"
)

type UserDeleterRepository interface {
	DeleteUser(context.Context, string) error
	DeleteAllUserCaches(string) error
}

type userDeleterRepository struct{
	db *gorm.DB
	cache cache.Cacheable
}

func NewUserDeleterRepository(db *gorm.DB, cache cache.Cacheable) UserDeleterRepository {
	return &userDeleterRepository{
		db: db,
		cache: cache,
	}
}

func (u *userDeleterRepository) DeleteUser(ctx context.Context, id string) error {
	if err := u.db.WithContext(ctx).Where("id = ?", id).Delete(&entity.User{}).Error; err != nil {
		return utils.NewCustomError(500, "Error deleting user")
	}
	return nil
}

func (u *userDeleterRepository) DeleteAllUserCaches(refID string) error {
	err := u.cache.BulkRemove(fmt.Sprintf(cache.UserDatatable, "*", "*", "*", "*", "*"))
	if err != nil {
		return err
	}

	err = u.cache.BulkRemove(fmt.Sprintf(cache.UserDatatable, "*", "*", "*", "*", "*"))
	if err != nil {
		return err
	}

	if refID != "" {
		err = u.cache.Remove(fmt.Sprintf(cache.UserFindByID, refID))
		if err != nil {
			return err
		}
	}

	return nil
}
