package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"gin-restful-starter/shared/cache"
	"gin-restful-starter/shared/constant"
	"gin-restful-starter/shared/dto"
	"gin-restful-starter/shared/tools"
	"gin-restful-starter/shared/utils"
	"gorm.io/gorm"
	"gin-restful-starter/modules/user/entity"
	"strings"
)

type UserFinderRepository interface {
	GetUserByID(ctx context.Context, refID string) (*entity.User, error)
	GetUserByEmail(ctx context.Context, email string) (*entity.User, error)
	GetAllUsers(ctx context.Context, pagination dto.Pagination) ([]*entity.User, int64, error)
}

type userFinderRepository struct {
	db *gorm.DB
	cache cache.Cacheable
}

func NewUserFinderRepository(db *gorm.DB, cache cache.Cacheable) UserFinderRepository {
	return &userFinderRepository{
		db: db,
		cache: cache,
	}
}

func (r *userFinderRepository) GetUserByID(ctx context.Context, refID string) (*entity.User, error) {
	var result entity.User
	userBytes, err := r.cache.Get(fmt.Sprintf(cache.UserFindByID, refID))
	if err != nil {
		return nil, err
	}

	if userBytes != nil {
		_ = json.Unmarshal(userBytes, &result)
		return &result, nil
	}

	if err := r.db.WithContext(ctx).Where("id = ?", refID).First(&result).Error; err != nil {
		return nil, utils.NewCustomError(404, "User not found")
	}

	err = r.cache.Set(fmt.Sprintf(cache.UserFindByID, refID), result, constant.OneHourInSecond)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (r *userFinderRepository) GetUserByEmail(ctx context.Context, email string) (*entity.User, error) {
	var result entity.User
	userBytes, err := r.cache.Get(fmt.Sprintf(cache.UserFindByEmail, email))
	if err != nil {
		return nil, err
	}

	if userBytes != nil {
		_ = json.Unmarshal(userBytes, &result)
		return &result, nil
	}

	if err := r.db.WithContext(ctx).Where("email = ?", email).First(&result).Error; err != nil {
		return nil, utils.NewCustomError(404, "User not found")
	}

	err = r.cache.Set(fmt.Sprintf(cache.UserFindByID, email), result, constant.OneHourInSecond)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (r *userFinderRepository) GetAllUsers(ctx context.Context, pagination dto.Pagination) ([]*entity.User, int64, error) {
	users := make([]*entity.User, 0)
	var count int64

	dataTable, err := r.cache.Get(fmt.Sprintf(cache.UserDatatable, pagination.Search, pagination.Page, pagination.Limit, pagination.OrderBy, pagination.OrderColumn))
	if err != nil {
		return nil, 0, err
	}

	dataTableCount, err := r.cache.Get(fmt.Sprintf(cache.UserDatatable, pagination.Search, pagination.Page, pagination.Limit, pagination.OrderBy, pagination.OrderColumn))
	if err != nil {
		return nil, 0, err
	}

	if dataTable != nil && dataTableCount != nil {
		_ = json.Unmarshal(dataTable, &users)
		_ = json.Unmarshal(dataTableCount, &count)
		return users, count, nil
	}

	offset := (pagination.Page - 1) * pagination.Limit
	query := r.db.WithContext(ctx).Model(&entity.User{})

	if pagination.Search != "" {
		query = query.Where("name ILIKE ?", "%" + pagination.Search + "%")
	}

	if strings.ToLower(pagination.OrderColumn) != "" && pagination.OrderBy != "" {
		query = query.Order(tools.EscapeSpecial(pagination.OrderColumn) + " " + tools.EscapeSpecial(pagination.OrderBy))
	} else {
		query = query.Order("created_at DESC")
	}

	query.Count(&count)
	if err := query.
		Limit(int(pagination.Limit)).
		Offset(int(offset)).
		Set("gorm:auto_preload", true).
		Find(&users).
		Error; err != nil {
		return nil, 0, err
	}

	err = r.cache.Set(fmt.Sprintf(cache.UserDatatable, pagination.Search, pagination.Page, pagination.Limit, pagination.OrderBy, pagination.OrderColumn), users, constant.OneHourInSecond)
	if err != nil {
		return nil, 0, err
	}
	err = r.cache.Set(fmt.Sprintf(cache.UserDatatable, pagination.Search, pagination.Page, pagination.Limit, pagination.OrderBy, pagination.OrderColumn), count, constant.OneHourInSecond)
	if err != nil {
		return nil, 0, err
	}

	return users, count, nil
}
