package repository

import (
	"context"
	customErrors "gin-restful-starter/errors"
	"gin-restful-starter/modules/user/entity"
	"gorm.io/gorm"
	"gin-restful-starter/shared/cache"
)

type UserCreatorRepository interface {
	CreateUser(ctx context.Context, user *entity.User) error
}

type userCreatorRepository struct{
	db *gorm.DB
	cache cache.Cacheable
}

func NewUserCreatorRepository(db *gorm.DB, cache cache.Cacheable,) UserCreatorRepository {
	return &userCreatorRepository{
		db: db,
		cache: cache,
	}
}

func (u *userCreatorRepository) CreateUser(ctx context.Context, user *entity.User) error {
	if err := u.db.WithContext(ctx).Create(user).Error; err != nil {
		return customErrors.NewCustomError(500, err.Error())
	}

	return nil
}
