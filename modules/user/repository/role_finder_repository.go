package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"gin-restful-starter/shared/cache"
	"gin-restful-starter/shared/constant"
	"gin-restful-starter/shared/dto"
	"gin-restful-starter/shared/tools"
	"gin-restful-starter/shared/utils"
	"gorm.io/gorm"
	"gin-restful-starter/modules/user/entity"
	"strings"
)

type RoleFinderRepository interface {
	GetRoleByID(ctx context.Context, id string) (*entity.Role, error)
	GetAllRoles(ctx context.Context, pagination dto.Pagination) ([]*entity.Role, int64, error)
}

type roleFinderRepository struct {
	db *gorm.DB
	cache cache.Cacheable
}

func NewRoleFinderRepository(
	db *gorm.DB,
	cache cache.Cacheable,
) RoleFinderRepository {
	return &roleFinderRepository{
		db: db,
		cache: cache,
	}
}

func (r *roleFinderRepository) GetRoleByID(ctx context.Context, refID string) (*entity.Role, error) {
	var role entity.Role

	roleBytes, err := r.cache.Get(fmt.Sprintf(cache.RoleFindByID, refID))
	if err != nil {
		return nil, err
	}

	if roleBytes != nil {
		_ = json.Unmarshal(roleBytes, &role)
		return &role, nil
	}

	if err := r.db.WithContext(ctx).Where("id = ?", refID).First(&role).Error; err != nil {
		return nil, utils.NewCustomError(404, "role not found")
	}

	err = r.cache.Set(fmt.Sprintf(cache.RoleFindByID, refID), role, constant.OneHourInSecond)
	if err != nil {
		return nil, err
	}

	return &role, nil
}

func (r *roleFinderRepository) GetAllRoles(ctx context.Context, pagination dto.Pagination) ([]*entity.Role, int64, error) {
	var roles []*entity.Role
	var count int64

	dataTable, err := r.cache.Get(fmt.Sprintf(cache.RoleDatatable, pagination.Search, pagination.Page, pagination.Limit, pagination.OrderBy, pagination.OrderColumn))
	if err != nil {
		return nil, 0, err
	}

	dataTableCount, err := r.cache.Get(fmt.Sprintf(cache.RoleDatatable, pagination.Search, pagination.Page, pagination.Limit, pagination.OrderBy, pagination.OrderColumn))
	if err != nil {
		return nil, 0, err
	}

	if dataTable != nil && dataTableCount != nil {
		_ = json.Unmarshal(dataTable, &roles)
		_ = json.Unmarshal(dataTableCount, &count)
		return roles, count, nil
	}

	offset := (pagination.Page - 1) * pagination.Limit

	query := r.db.WithContext(ctx).WithContext(ctx).Model(&entity.Role{})

	if pagination.Search != "" {
		query = query.Where("name ILIKE ?", "%" + pagination.Search + "%")
	}

	if strings.ToLower(pagination.OrderColumn) != "" && pagination.OrderBy != "" {
		query = query.Order(tools.EscapeSpecial(pagination.OrderColumn) + " " + tools.EscapeSpecial(pagination.OrderBy))
	} else {
		query = query.Order("created_at DESC")
	}

	query.Count(&count)
	if err := query.
		Limit(int(pagination.Limit)).
		Offset(int(offset)).
		Set("gorm:auto_preload", true).
		Find(&roles).
		Error; err != nil {
		return nil, 0, err
	}

	err = r.cache.Set(fmt.Sprintf(cache.UserDatatable, pagination.Search, pagination.Page, pagination.Limit, pagination.OrderBy, pagination.OrderColumn), roles, constant.OneHourInSecond)
	if err != nil {
		return nil, 0, err
	}
	err = r.cache.Set(fmt.Sprintf(cache.UserDatatable, pagination.Search, pagination.Page, pagination.Limit, pagination.OrderBy, pagination.OrderColumn), count, constant.OneHourInSecond)
	if err != nil {
		return nil, 0, err
	}

	return roles, count, nil
}
