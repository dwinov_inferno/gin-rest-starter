package repository

import (
	"context"
	"errors"
	userEntity "gin-restful-starter/modules/user/entity"
	"gin-restful-starter/shared/constant"
	"gin-restful-starter/shared/dto"
	"github.com/stretchr/testify/mock"
)

type UserFinderServiceMock struct {
	mock.Mock
}

func (m *UserFinderServiceMock) GetUserByEmail(ctx context.Context, email string) (*userEntity.User, error) {
	args := m.Called(ctx, email)
	if args.Get(constant.Zero) == nil {
		return nil, errors.New("user not found")
	}

	result := args.Get(0).(userEntity.User)

	return &result, nil
}

func (m *UserFinderServiceMock) GetUserByID(ctx context.Context, id string) (*userEntity.User, error) {
	args := m.Called(ctx, id)
	if args.Get(constant.Zero) == nil {
		return nil, errors.New("user not found")
	}

	result := args.Get(0).(userEntity.User)

	return &result, nil
}

func (m *UserFinderServiceMock) GetAllUsers(ctx context.Context, pagination dto.Pagination) ([]*userEntity.User, int64, error) {
	args := m.Called(ctx, pagination)
	return args.Get(0).([]*userEntity.User), int64(args.Get(1).(int)), args.Error(2)
}
