package handler

import (
	"gin-restful-starter/modules/user/service"
	"gin-restful-starter/shared/constant"
	"gin-restful-starter/shared/dto"
	"gin-restful-starter/shared/tools"
	"gin-restful-starter/shared/utils"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type UserFinderHandler struct {
	userFinderService service.UserFinderService
}

func NewUserFinderHandler(
	userFinderService service.UserFinderService,
) *UserFinderHandler {
	return &UserFinderHandler{
		userFinderService: userFinderService,
	}
}

func (h *UserFinderHandler) GetUserByID(c *gin.Context) {
	id := tools.EscapeSpecial(c.Param("id"))
	ctx := c.Request.Context()

	user, err := h.userFinderService.GetUserByID(ctx, id)
	if err != nil {
		utils.ErrorHandler(c, err)
		return
	}

	c.JSON(http.StatusOK, dto.SuccessJSON{
		Code:    http.StatusOK,
		Message: constant.SuccessMessage,
		Data:    user,
	})
}

func (h *UserFinderHandler) GetAllUsers(c *gin.Context) {
	ctx := c.Request.Context()

	page64, err := strconv.ParseUint(c.Param("page"), 10, 32)
	if err != nil {
		log.Fatalf("Error converting string to uint32: %v", err)
	}

	limit64, err := strconv.ParseUint(c.Param("limit"), 10, 32)
	if err != nil {
		log.Fatalf("Error converting string to uint32: %v", err)
	}

	pagination := dto.Pagination{
		Search:      tools.EscapeSpecial(c.Param("search")),
		Page:        uint32(page64),
		Limit:       uint32(limit64),
		OrderBy:     tools.EscapeSpecial(c.Param("order")),
		OrderColumn: tools.EscapeSpecial(c.Param("column")),
	}
	c.Param("id")
	users, count, err := h.userFinderService.GetAllUsers(ctx, pagination)
	if err != nil {
		utils.ErrorHandler(c, err)
		return
	}

	totalPage := float64(count) / float64(pagination.Limit)
	c.JSON(http.StatusOK, dto.SuccessJSONDatatable{
		TotalPage: uint32(totalPage),
		Count:     uint32(count),
		Page:      pagination.Page,
		Data:      users,
	})
}
