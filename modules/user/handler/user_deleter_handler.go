package handler

import (
	"gin-restful-starter/modules/user/service"
	"gin-restful-starter/shared/constant"
	"gin-restful-starter/shared/dto"
	"gin-restful-starter/shared/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

type UserDeleterHandler struct {
	userDeleterService service.UserDeleterService
}

func NewUserDeleterHandler(
	userDeleterService service.UserDeleterService,
) *UserDeleterHandler {
	return &UserDeleterHandler{
		userDeleterService: userDeleterService,
	}
}

func (h *UserDeleterHandler) DeleteUser(c *gin.Context) {
	ctx := c.Request.Context()

	id := c.Param("id")

	err := h.userDeleterService.DeleteUser(ctx, id)
	if err != nil {
		utils.ErrorHandler(c, err)
		return
	}

	c.JSON(http.StatusOK, dto.SuccessJSON{
		Data: http.StatusOK,
		Message: constant.SuccessMessage,
	})
}
