package handler

import (
	"gin-restful-starter/modules/user/entity"
	"gin-restful-starter/modules/user/service"
	"gin-restful-starter/shared/constant"
	"gin-restful-starter/shared/dto"
	"gin-restful-starter/shared/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

type UserUpdaterHandler struct {
	userUpdaterService service.UserUpdaterService
}

func NewUserUpdaterHandler(
	userUpdaterService service.UserUpdaterService,
) *UserUpdaterHandler {
	return &UserUpdaterHandler{
		userUpdaterService: userUpdaterService,
	}
}

func (h *UserUpdaterHandler) UpdateUser(c *gin.Context) {
	ctx := c.Request.Context()
	var req entity.User

	id := c.Param("id")

	if err := c.ShouldBindJSON(&req); err != nil {
		utils.ErrorHandler(c, utils.NewCustomError(400, "Invalid request"))
		return
	}

	err := h.userUpdaterService.UpdateUser(ctx, id, &req)
	if err != nil {
		utils.ErrorHandler(c, err)
		return
	}

	c.JSON(http.StatusOK, dto.SuccessJSON{
		Code: http.StatusOK,
		Message: constant.SuccessMessage,
	})
}
