package handler

import (
	"gin-restful-starter/modules/user/entity"
	"gin-restful-starter/modules/user/service"
	"gin-restful-starter/shared/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

type UserCreatorHandler struct {
	userCreatorService service.UserCreatorService
}

func NewUserCreatorHandler(
	userCreatorService service.UserCreatorService,
	) *UserCreatorHandler {
	return &UserCreatorHandler{
		userCreatorService: userCreatorService,
	}
}

func (h *UserCreatorHandler) CreateUser(c *gin.Context) {
	ctx := c.Request.Context()
	var req entity.User
	if err := c.ShouldBindJSON(&req); err != nil {
		utils.ErrorHandler(c, utils.NewCustomError(400, "Invalid request"))
		return
	}

	err := h.userCreatorService.CreateUser(ctx, &req)
	if err != nil {
		utils.ErrorHandler(c, err)
		return
	}

	c.JSON(http.StatusCreated, gin.H{"message": "User created"})
}
