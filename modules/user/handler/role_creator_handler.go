package handler

import (
	"gin-restful-starter/modules/user/entity"
	"gin-restful-starter/modules/user/service"
	"gin-restful-starter/shared/constant"
	"gin-restful-starter/shared/dto"
	"gin-restful-starter/shared/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

type RoleCreatorHandler struct {
	roleCreatorService service.RoleCreatorService
}

func NewRoleCreatorHandler(
	roleCreatorService service.RoleCreatorService,
) *RoleCreatorHandler {
	return &RoleCreatorHandler{
		roleCreatorService: roleCreatorService,
	}
}


func (h *RoleCreatorHandler) CreateRole(c *gin.Context) {
	ctx := c.Request.Context()
	var req entity.Role
	if err := c.ShouldBindJSON(&req); err != nil {
		utils.ErrorHandler(c, utils.NewCustomError(400, "Invalid request"))
		return
	}

	err := h.roleCreatorService.CreateRole(ctx, &req)
	if err != nil {
		utils.ErrorHandler(c, err)
		return
	}

	c.JSON(http.StatusCreated, dto.SuccessJSON{
		Code: http.StatusOK,
		Message: constant.SuccessMessage,
	})
}
