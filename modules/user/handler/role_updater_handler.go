package handler

import (
	"gin-restful-starter/modules/user/entity"
	"gin-restful-starter/modules/user/service"
	"gin-restful-starter/shared/constant"
	"gin-restful-starter/shared/dto"
	"gin-restful-starter/shared/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

type RoleUpdaterHandler struct {
	roleUpdaterService service.RoleUpdaterService
}

func NewRoleUpdaterHandler(
	roleUpdaterService service.RoleUpdaterService,
) *RoleUpdaterHandler {
	return &RoleUpdaterHandler{
		roleUpdaterService: roleUpdaterService,
	}
}

func (h *RoleUpdaterHandler) UpdateRole(c *gin.Context) {
	ctx := c.Request.Context()
	var req entity.Role

	id := c.Param("id")

	if err := c.ShouldBindJSON(&req); err != nil {
		utils.ErrorHandler(c, utils.NewCustomError(400, "Invalid request"))
		return
	}

	err := h.roleUpdaterService.UpdateRole(ctx, id, &req)
	if err != nil {
		utils.ErrorHandler(c, err)
		return
	}

	c.JSON(http.StatusOK, dto.SuccessJSON{
		Code: http.StatusOK,
		Message: constant.SuccessMessage,
	})
}
