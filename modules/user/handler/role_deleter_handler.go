package handler

import (
	"gin-restful-starter/modules/user/service"
	"gin-restful-starter/shared/constant"
	"gin-restful-starter/shared/dto"
	"gin-restful-starter/shared/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

type RoleDeleterHandler struct {
	roleDeleterService service.RoleDeleterService
}

func NewRoleDeleterHandler(
	roleDeleterService service.RoleDeleterService,
) *RoleDeleterHandler {
	return &RoleDeleterHandler{
		roleDeleterService: roleDeleterService,
	}
}

func (h *RoleDeleterHandler) DeleteRole(c *gin.Context) {
	ctx := c.Request.Context()

	id := c.Param("id")

	err := h.roleDeleterService.DeleteRole(ctx, id)
	if err != nil {
		utils.ErrorHandler(c, err)
		return
	}

	c.JSON(http.StatusOK, dto.SuccessJSON{
		Data: http.StatusOK,
		Message: constant.SuccessMessage,
	})
}
