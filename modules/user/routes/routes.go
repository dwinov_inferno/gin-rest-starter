package routes

import (
	"gin-restful-starter/middleware"
	authRepository "gin-restful-starter/modules/authentication/repository"
	userHandler "gin-restful-starter/modules/user/handler"
	userRepo "gin-restful-starter/modules/user/repository"
	userService "gin-restful-starter/modules/user/service"
	commonredis "gin-restful-starter/shared/redis"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"github.com/gomodule/redigo/redis"
)

func UserRoutes(router *gin.Engine, db *gorm.DB, redisPool *redis.Pool) {
	// Cache
	cache := commonredis.NewClient(redisPool)

	// Initialize user repositories
	userCreatorRepository := userRepo.NewUserCreatorRepository(db, cache)
	userFinderRepository := userRepo.NewUserFinderRepository(db, cache)
	userUpdaterRepository := userRepo.NewUserUpdaterRepository(db, cache)
	userDeleterRepository := userRepo.NewUserDeleterRepository(db, cache)

	// Initialize role repositories
	roleCreatorRepository := userRepo.NewRoleCreatorRepository(db, cache)
	roleFinderRepository := userRepo.NewRoleFinderRepository(db, cache)
	roleUpdaterRepository := userRepo.NewRoleUpdaterRepository(db, cache)
	roleDeleterRepository := userRepo.NewRoleDeleterRepository(db, cache)

	// Initialize user services
	userCreatorService := userService.NewUserCreatorService(userCreatorRepository, userDeleterRepository)
	userFinderService := userService.NewUserFinderService(userFinderRepository)
	userUpdaterService := userService.NewUserUpdaterService(userUpdaterRepository, userDeleterRepository)
	userDeleterService := userService.NewUserDeleterService(userDeleterRepository)

	// Initialize role services
	roleCreatorService := userService.NewRoleCreatorService(roleCreatorRepository, roleDeleterRepository)
	roleFinderService := userService.NewRoleFinderService(roleFinderRepository)
	roleUpdaterService := userService.NewRoleUpdaterService(roleUpdaterRepository, roleDeleterRepository)
	roleDeleterService := userService.NewRoleDeleterService(roleDeleterRepository)

	// Initialize user handlers
	userCreatorHandler := userHandler.NewUserCreatorHandler(userCreatorService)
	userFinderHandler := userHandler.NewUserFinderHandler(userFinderService)
	userUpdaterHandler := userHandler.NewUserUpdaterHandler(userUpdaterService)
	userDeleterHandler := userHandler.NewUserDeleterHandler(userDeleterService)

	// Initialize role handlers
	roleCreatorHandler := userHandler.NewRoleCreatorHandler(roleCreatorService)
	roleFinderHandler := userHandler.NewRoleFinderHandler(roleFinderService)
	roleUpdaterHandler := userHandler.NewRoleUpdaterHandler(roleUpdaterService)
	roleDeleterHandler := userHandler.NewRoleDeleterHandler(roleDeleterService)

	user := router.Group("/user")
	var authRepo authRepository.AuthRepository
	user.Use(middleware.AuthMiddleware(authRepo))
	{
		user.POST("/create", userCreatorHandler.CreateUser)
		user.GET("/find-by-id", userFinderHandler.GetUserByID)
		user.GET("/find-all", userFinderHandler.GetAllUsers)
		user.PUT("/update", userUpdaterHandler.UpdateUser)
		user.DELETE("/delete", userDeleterHandler.DeleteUser)

		user.POST("/role/create", roleCreatorHandler.CreateRole)
		user.GET("/role/find-by-id", roleFinderHandler.GetRoleByID)
		user.GET("/role/find-all", roleFinderHandler.GetAllRoles)
		user.PUT("/role/update", roleUpdaterHandler.UpdateRole)
		user.DELETE("/role/delete", roleDeleterHandler.DeleteRole)
	}
}
