package service

import (
	"context"
	"gin-restful-starter/modules/user/repository"
	"log"
)

type RoleDeleterService interface {
	DeleteRole(ctx context.Context, id string) (error)
}

type roleDeleterService struct {
	roleDeleterRepository repository.RoleDeleterRepository
}

func NewRoleDeleterService(
	roleDeleterRepository repository.RoleDeleterRepository,
) RoleDeleterService {
	return &roleDeleterService{
		roleDeleterRepository: roleDeleterRepository,
	}
}

func (s *roleDeleterService) DeleteRole(ctx context.Context, id string) (error) {
	err := s.roleDeleterRepository.DeleteRole(ctx, id)
	if err != nil {
		log.Print("[RoleDeleterService - DeleteRole] Error while deleting role: ", err)
		return err
	}

	err = s.roleDeleterRepository.DeleteAllRoleCaches(id)
	if err != nil {
		log.Print("[RoleDeleterService - DeleteRole] Error while deleting cache: ", err)
		return err
	}

	return nil
}
