package service

import (
	"context"
	"gin-restful-starter/modules/user/repository"
	"log"
)

type UserDeleterService interface {
	DeleteUser(ctx context.Context, id string) (error)
}

type userDeleterService struct {
	userDeleterRepository repository.UserDeleterRepository
}

func NewUserDeleterService(userDeleterRepository repository.UserDeleterRepository) UserDeleterService {
	return &userDeleterService{
		userDeleterRepository: userDeleterRepository,
	}
}

func (s *userDeleterService) DeleteUser(ctx context.Context, id string) (error) {
	err := s.userDeleterRepository.DeleteUser(ctx, id)
	if err != nil {
		log.Print("[UserDeleterService - DeleteUser] Error while deleting user: ", err)
		return err
	}

	err = s.userDeleterRepository.DeleteAllUserCaches(id)
	if err != nil {
		log.Print("[UserDeleterService - DeleteUser] Error while deleting user: ", err)
		return err
	}

	return nil
}
