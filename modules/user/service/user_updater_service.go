package service

import (
	"context"
	"gin-restful-starter/modules/user/entity"
	"gin-restful-starter/modules/user/repository"
	"log"
)

type UserUpdaterService interface {
	UpdateUser(ctx context.Context, id string, user *entity.User) (error)
}

type userUpdaterService struct {
	userUpdaterRepository repository.UserUpdaterRepository
	userDeleterRepository repository.UserDeleterRepository
}

func NewUserUpdaterService(
	userUpdaterRepository repository.UserUpdaterRepository,
	userDeleterRepository repository.UserDeleterRepository,
) UserUpdaterService {
	return &userUpdaterService{
		userUpdaterRepository: userUpdaterRepository,
		userDeleterRepository: userDeleterRepository,
	}
}

func (s *userUpdaterService) UpdateUser(ctx context.Context, id string, user *entity.User) (error) {
	err := s.userUpdaterRepository.UpdateUser(ctx, id, user)
	if err != nil {
		log.Print("[UserUpdaterService - UpdateUser] Error while updating user: ", err)
		return err
	}

	err = s.userDeleterRepository.DeleteAllUserCaches(id)
	if err != nil {
		log.Print("[UserUpdaterService - UpdateUser] Error while deleting cache: ", err)
		return err
	}

	return nil
}
