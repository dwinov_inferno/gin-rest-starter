package service

import (
	"context"
	"gin-restful-starter/modules/user/entity"
	"gin-restful-starter/modules/user/repository"
	"gin-restful-starter/shared/utils"
	"github.com/google/uuid"
	"log"
)

type UserCreatorService interface {
	CreateUser(ctx context.Context, req *entity.User) error
}

type userCreatorService struct {
	userCreatorRepository repository.UserCreatorRepository
	userDeleterRepository repository.UserDeleterRepository
}

func NewUserCreatorService(
	userCreatorRepository repository.UserCreatorRepository,
	userDeleterRepository repository.UserDeleterRepository,
) UserCreatorService {
	return &userCreatorService{
		userCreatorRepository: userCreatorRepository,
		userDeleterRepository: userDeleterRepository,
	}
}

func (s *userCreatorService) CreateUser(ctx context.Context, req *entity.User) error {
	hashedPassword, err := utils.HashPassword(req.Password)
	if err != nil {
		log.Print("[UserCreatorService - CreateUser] Error while hashing password: ", err)
		return utils.NewCustomError(500, "Error hashing password")
	}

	user := &entity.User{
		ID:       uuid.New(),
		Email:    req.Email,
		Password: hashedPassword,
	}

	err = s.userCreatorRepository.CreateUser(ctx, user)
	if err != nil {
		log.Print("[UserCreatorService - CreateUser] Error while creating user: ", err)
		return utils.NewCustomError(500, "Error creating user")
	}

	err = s.userDeleterRepository.DeleteAllUserCaches("")
	if err != nil {
		log.Print("[UserCreatorService - CreateUser] Error while deleting cache: ", err)
		return err
	}

	return nil
}
