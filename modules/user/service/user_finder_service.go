package service

import (
	"context"
	"gin-restful-starter/modules/user/entity"
	"gin-restful-starter/modules/user/repository"
	"gin-restful-starter/shared/dto"
	"gin-restful-starter/shared/errors"
	"gorm.io/gorm"
	"log"
)

type UserFinderService interface {
	GetUserByID(ctx context.Context, id string) (*entity.User, error)
	GetAllUsers(ctx context.Context, pagination dto.Pagination) ([]*entity.User, int64, error)
	GetUserByEmail(ctx context.Context, email string) (*entity.User, error)
}

type userFinderService struct {
	userFinderRepository repository.UserFinderRepository
}

func NewUserFinderService(userFinderRepository repository.UserFinderRepository) UserFinderService {
	return &userFinderService{
		userFinderRepository: userFinderRepository,
	}
}

func (s *userFinderService) GetUserByID(ctx context.Context, id string) (*entity.User, error) {
	user, err := s.userFinderRepository.GetUserByID(ctx, id)
	if err != nil {
		log.Print("[UserFinderService - GetUserByID] Error while get user by id: ", err)
		return nil, err
	}
	return user, nil
}

func (s *userFinderService) GetUserByEmail(ctx context.Context, email string) (*entity.User, error) {
	user, err := s.userFinderRepository.GetUserByEmail(ctx, email)
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.ErrRecordNotFound.Error()
		}
		log.Print("[UserFinderService - GetUserByEmail] Error while user by email: ", err)
		return nil, err
	}
	return user, nil
}

func (s *userFinderService) GetAllUsers(ctx context.Context, pagination dto.Pagination) ([]*entity.User, int64, error) {
	users, count, err := s.userFinderRepository.GetAllUsers(ctx, pagination)
	if err != nil {
		log.Print("[UserFinderService - GetAllUsers] Error while find users for datatable: ", err)
		return nil, 0, err
	}
	return users, count, nil
}
