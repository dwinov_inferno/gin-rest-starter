package service

import (
	"context"
	"gin-restful-starter/modules/user/entity"
	"gin-restful-starter/modules/user/repository"
	"gin-restful-starter/shared/dto"
	"log"
)

type RoleFinderService interface {
	GetRoleByID(ctx context.Context, refID string) (*entity.Role, error)
	GetAllRoles(ctx context.Context, pagination dto.Pagination) ([]*entity.Role, int64, error)
}

type roleFinderService struct {
	roleFinderRepository repository.RoleFinderRepository
}

func NewRoleFinderService(
	roleFinderRepository repository.RoleFinderRepository,
) RoleFinderService {
	return &roleFinderService{
		roleFinderRepository: roleFinderRepository,
	}
}

func (s *roleFinderService) GetRoleByID(ctx context.Context, id string) (*entity.Role, error) {
	user, err := s.roleFinderRepository.GetRoleByID(ctx, id)
	if err != nil {
		log.Print("[RoleFinderCreator - GetRoleByID] Error while finding role by id: ", err)
		return nil, err
	}
	return user, nil
}

func (s *roleFinderService) GetAllRoles(ctx context.Context, pagination dto.Pagination) ([]*entity.Role, int64, error) {
	users, count, err := s.roleFinderRepository.GetAllRoles(ctx, pagination)
	if err != nil {
		log.Print("[RoleFinderService - GetAllRoles] Error while finding roles for datatable: ", err)
		return nil, 0, err
	}
	return users, count, nil
}
