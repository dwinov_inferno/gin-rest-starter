package service

import (
	"context"
	"gin-restful-starter/modules/user/entity"
	"gin-restful-starter/modules/user/repository"
	"gin-restful-starter/shared/utils"
	"github.com/google/uuid"
	"log"
)

type RoleCreatorService interface {
	CreateRole(ctx context.Context, req *entity.Role) error
}

type roleCreatorService struct {
	roleCreatorRepository repository.RoleCreatorRepository
	roleDeleterRepository repository.RoleDeleterRepository
}

func NewRoleCreatorService(
	roleCreatorRepository repository.RoleCreatorRepository,
	roleDeleterRepository repository.RoleDeleterRepository,
) RoleCreatorService {
	return &roleCreatorService{
		roleCreatorRepository: roleCreatorRepository,
		roleDeleterRepository: roleDeleterRepository,
	}
}

func (s *roleCreatorService) CreateRole(ctx context.Context, req *entity.Role) error {
	role := &entity.Role{
		ID:          uuid.New(),
		Name:        req.Name,
		Description: req.Description,
	}

	err := s.roleCreatorRepository.CreateRole(ctx, role)
	if err != nil {
		log.Print("[RoleCreatorService - CreateRole] Error while creating role: ", err)
		return utils.NewCustomError(500, "error creating role")
	}

	err = s.roleDeleterRepository.DeleteAllRoleCaches("")
	if err != nil {
		log.Print("[RoleCreatorService - CreateRole] Error while deleting cache: ", err)
		return err
	}

	return nil
}
