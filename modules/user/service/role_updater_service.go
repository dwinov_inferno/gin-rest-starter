package service

import (
	"context"
	"gin-restful-starter/modules/user/entity"
	"gin-restful-starter/modules/user/repository"
	"log"
)

type RoleUpdaterService interface {
	UpdateRole(ctx context.Context, id string, role *entity.Role) (error)
}

type roleUpdaterService struct {
	roleUpdaterRepository repository.RoleUpdaterRepository
	roleDeleterRepository repository.RoleDeleterRepository
}

func NewRoleUpdaterService(
	roleUpdaterRepository repository.RoleUpdaterRepository,
	roleDeleterRepository repository.RoleDeleterRepository,
) RoleUpdaterService {
	return &roleUpdaterService{
		roleUpdaterRepository: roleUpdaterRepository,
		roleDeleterRepository: roleDeleterRepository,
	}
}

func (s *roleUpdaterService) UpdateRole(ctx context.Context, id string, role *entity.Role) (error) {
	err := s.roleUpdaterRepository.UpdateRole(ctx, id, role)
	if err != nil {
		log.Print("[RoleUpdaterService - UpdateRole] Error while updating role: ", err)
		return err
	}

	err = s.roleDeleterRepository.DeleteAllRoleCaches(id)
	if err != nil {
		log.Print("[RoleUpdaterService - UpdateRole] Error while deleting role: ", err)
		return err
	}

	return nil
}
