package entity

import (
	"github.com/google/uuid"
	"time"
)

// User model
type User struct {
	ID          uuid.UUID `gorm:"type:uuid;primary_key"`
	Username    string    `json:"username"`
	Email       string    `gorm:"unique"`
	RoleID      uuid.UUID `json:"role_id"`
	Password    string    `json:"password"`
	PhoneNumber string    `json:"phone_number"`
	CreatedAt   time.Time `json:"created_at"`
	CreatedBy   string    `json:"created_by"`
	UpdatedAt   time.Time `json:"updated_at"`
	UpdatedBy   string    `json:"updated_by"`
	DeletedAt   time.Time `json:"deleted_at"`
	DeletedBy   string    `json:"deleted_by"`
}
