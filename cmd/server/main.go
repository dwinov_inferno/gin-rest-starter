package main

import (
	"fmt"
	"github.com/gomodule/redigo/redis"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"context"

	"gin-restful-starter/config"
	"gin-restful-starter/routes"
	commonRedis "gin-restful-starter/shared/redis"
)

const (
	version            = "1.0.0"
)

func splash(cfg *config.Config) {
	colorReset := "\033[0m"
	colorBlue := "\033[34m"

	fmt.Printf(`        .__                                          __    _____     .__                      __                 __                
   ____ |__| ____           _______   ____   _______/  |__/ ____\_ __|  |             _______/  |______ ________/  |_  ___________ 
  / ___\|  |/    \   ______ \_  __ \_/ __ \ /  ___/\   __\   __\  |  \  |    ______  /  ___/\   __\__  \\_  __ \   __\/ __ \_  __ \
 / /_/  >  |   |  \ /_____/  |  | \/\  ___/ \___ \  |  |  |  | |  |  /  |__ /_____/  \___ \  |  |  / __ \|  | \/|  | \  ___/|  | \/
 \___  /|__|___|  /          |__|    \___  >____  > |__|  |__| |____/|____/         /____  > |__| (____  /__|   |__|  \___  >__|   
/_____/         \/                       \/     \/                                       \/            \/                 \/        v%s
`, version)

	fmt.Println(colorBlue, fmt.Sprintf(`⇨ REST server started on :%s`, cfg.Port.PORT))
	fmt.Println(colorReset, "")
}

func main() {
	// Load config
	cfg, err := config.LoadConfig(".env")
	if err != nil {
		log.Fatalf("Could not load config: %v", err)
	}

	splash(cfg)

	config.ConnectDatabase(cfg)

	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Shanghai",
		cfg.Postgres.Host, cfg.Postgres.User, cfg.Postgres.Password, cfg.Postgres.Name, cfg.Postgres.Port,
	)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalf("Failed to connect to database: %v", err)
	}

	rdb := buildRedisPool(cfg)

	router := routes.SetupRouter(*cfg, db, rdb)
	if err := router.Run(":" + cfg.Port.PORT); err != nil {
		log.Fatalf("Failed to run server: %v", err)
	}
}

func buildRedisPool(cfg *config.Config) *redis.Pool {
	cachePool := commonRedis.NewPool(cfg.Redis.Address, cfg.Redis.Password)

	ctx := context.Background()
	_, err := cachePool.GetContext(ctx)

	if err != nil {
		checkError(err)
	}

	log.Print("redis successfully connected!")
	return cachePool
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}