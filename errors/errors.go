package errors

import (
	"log"
	"net/http"
)

type CustomError struct {
	Code    int    `json:"-"`
	Message string `json:"message"`
}

func (e *CustomError) Error() string {
	return e.Message
}

func NewCustomError(code int, message string) *CustomError {
	return &CustomError{
		Code:    code,
		Message: message,
	}
}

func HandleError(err error) (int, string) {
	if customErr, ok := err.(*CustomError); ok {
		log.Printf("Error: %v, Code: %d", customErr.Message, customErr.Code)
		return customErr.Code, customErr.Message
	}

	log.Printf("Error: %v", err)
	return http.StatusInternalServerError, "Internal Server Error"
}